package cz.marvatom.education.springcloud.ident.controller;

import cz.marvatom.education.springcloud.ident.model.IdentDto;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface ISBNApi {

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.TEXT_PLAIN_VALUE)
    ResponseEntity<IdentDto> createISBN();

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    List<IdentDto> getAll();

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<IdentDto> getISBNbyId(@PathVariable int id);

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.TEXT_PLAIN_VALUE)
    ResponseEntity<IdentDto> updateISBM(@PathVariable int id, @RequestBody String newValue);

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<IdentDto> deleteISBNbyId(@PathVariable int id);

}
