package cz.marvatom.education.springcloud.ident;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class IdentApplication {

    public static void main(String[] args) {
        SpringApplication.run(IdentApplication.class, args);
    }
}
