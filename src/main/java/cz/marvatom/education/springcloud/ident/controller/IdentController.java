package cz.marvatom.education.springcloud.ident.controller;

import cz.marvatom.education.springcloud.ident.model.IdentDto;
import cz.marvatom.education.springcloud.ident.service.IdnetService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class IdentController implements ISBNApi{

    IdnetService idnetService;

    public IdentController(IdnetService idnetService) {
        this.idnetService = idnetService;
    }

    @Override
    public ResponseEntity<IdentDto> createISBN() {
        return new ResponseEntity<>(idnetService.createNewISBN(), HttpStatus.CREATED);
    }

    @Override
    public List<IdentDto> getAll() {
        return idnetService.getAllISBN();
    }

    @Override
    public ResponseEntity<IdentDto> getISBNbyId(int id) {
        return idnetService.getISBNById(id).map((i) -> new ResponseEntity<>(i, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<IdentDto> updateISBM(int id, String newValue) {
        return new ResponseEntity<>(idnetService.updateISBN(id, newValue), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<IdentDto> deleteISBNbyId(int id) {
        return new ResponseEntity<>(idnetService.deleteISBN(id), HttpStatus.OK);
    }
}
