package cz.marvatom.education.springcloud.ident.service.imp;

import cz.marvatom.education.springcloud.ident.exception.EntityNotFoundException;
import cz.marvatom.education.springcloud.ident.model.IdentDto;
import cz.marvatom.education.springcloud.ident.service.IdnetService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class IdentServiceInMemoryImpl implements IdnetService {
    AtomicInteger idGen = new AtomicInteger(1);
    private List<IdentDto> idents = Collections.synchronizedList(new ArrayList<>());

    private static int MIN = 100000;
    private static int MAX = 200000;

    public IdentServiceInMemoryImpl() {
    }

    @Override
    public IdentDto createNewISBN() {
        String isbn = String.format("ISBN_%d", MIN + (int)(Math.random() * ((MAX - MIN) + 1)));
        IdentDto newIdent = new IdentDto(idGen.getAndAdd(1), isbn);
        synchronized (idents) {
            idents.add(newIdent);
        }
        return newIdent;
    }

    @Override
    public List<IdentDto> getAllISBN() {
        return idents;
    }

    @Override
    public Optional<IdentDto> getISBNById(int id) {
        synchronized (idents) {
            return idents.stream().filter((i) -> i.getId() == id).findAny();
        }
    }

    @Override
    public IdentDto updateISBN(int id, String newVal) {
        Optional<IdentDto> isbnById = getISBNById(id);
        if (!isbnById.isPresent()){
            throw new EntityNotFoundException(String.format("Book identity with ID %d doesn't exist.", id));
        }
        IdentDto identDto = isbnById.get();
        identDto.setISBN(newVal);
        return identDto;
    }

    @Override
    public IdentDto deleteISBN(int id) throws EntityNotFoundException {
        synchronized (idents) {
            Iterator<IdentDto> iterator = idents.listIterator();
            while (iterator.hasNext()) {
                IdentDto ident = iterator.next();
                if (ident.getId() == id) {
                    iterator.remove();
                    return ident;
                }
            }
            throw new EntityNotFoundException(String.format("Book identity with ID %d doesn't exist.", id));
        }
    }
}
