package cz.marvatom.education.springcloud.ident.model;

public class IdentDto {
    private int id;
    private String ISBN;

    public IdentDto() {
    }

    public IdentDto(int id, String ISBN) {
        this.id = id;
        this.ISBN = ISBN;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }
}
