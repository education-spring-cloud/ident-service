package cz.marvatom.education.springcloud.ident.service;

import cz.marvatom.education.springcloud.ident.exception.EntityNotFoundException;
import cz.marvatom.education.springcloud.ident.model.IdentDto;

import java.util.List;
import java.util.Optional;

public interface IdnetService {
    IdentDto createNewISBN();
    List<IdentDto> getAllISBN();
    Optional<IdentDto> getISBNById(int id);
    IdentDto updateISBN(int id, String newVal) throws EntityNotFoundException;
    IdentDto deleteISBN(int id) throws EntityNotFoundException;
}
