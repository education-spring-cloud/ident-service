# Book identity service

This service manages book identities (ISBNs).

## Changelog 
### Version 1.0
This version contains REST API for managing book identities. It's fully standalone and uses no other services.  

### Version 1.1
Registration within Eureka.

## Docker 
To build image from this directory:
```
docker build --tag gopas/ident-service .
```
And run it:
```
docker run -d --name ident-service -p7070:7070 gopas/ident-service
```
Stop it:
```
docker stop ident-service
```
Remove container:
```
docker rm ident-service
```